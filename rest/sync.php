<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/siil.inc';

    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/rest/groups.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/rest/lamps.php';

    define('LAMPS_LAST_ID_DEMO', 5045); // 6228 till 21 aug 2017

    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET');
            break;
        case "GET":
            header('Access-Control-Allow-Origin: *');
            if (isset($_GET['action'])) {
                // Process Action
                switch ($_GET['action']) {
                    case "siil":
                        $db_groups = [];
                        $last_group_version = "";
                        allGroups($db_groups, date("Y-m-d H:i:s", 0), $last_group_version);

                        $db_lamps = [];
                        $last_lamp_version = "";
                        allLamps($db_lamps, date("Y-m-d H:i:s", 0), $last_lamp_version);
                        $SIILInfo = getSIILInfo(array('cuadrillas' => array('groups' => $db_groups, 'rv' => $last_group_version), 'lamps' => array('qty' => count($db_lamps), 'rv' => $last_lamp_version)));

                        if (count($SIILInfo['cuadrillas']) > 0) {
                            db_fn_query("TRUNCATE TABLE mobile_groups");
                            foreach ($SIILInfo['cuadrillas'] as $cuadrilla) {
                                db_fn_query("INSERT INTO mobile_groups (id, name, alm_clave, cd_clave, edo_clave, pais_clave, row_version)
                                            VALUES (" . $cuadrilla['CLAVE'] . ", '" . $cuadrilla['DESCRIPCION'] . "', " . $cuadrilla['ALM_CLAVE'] . ", " . $cuadrilla['CD_CLAVE'] . ", " . $cuadrilla['EDO_CLAVE'] . ", " . $cuadrilla['PAIS_CLAVE'] . ", '" . date("Y-m-d H:i:s") . "')");
                            }
                        }

                        if (count($SIILInfo['lamps']) > 0) {
                            db_fn_query("UPDATE mobile_lamps SET active=0 WHERE id>" . LAMPS_LAST_ID_DEMO);
                            foreach ($SIILInfo['lamps'] as $lamp) {
                                if ($lamp['NUM_SERIE_ID'] > LAMPS_LAST_ID_DEMO) {
                                    if (!$lamp['POTENCIA'] || $lamp['POTENCIA'] == '') {
                                        preg_match_all('/[0-9]+W/', $lamp['ARTICULO'], $lamp['POTENCIA']);
                                        $lamp['POTENCIA'] = substr(end($lamp['POTENCIA'][0]), 0, -1);
                                        if (!$lamp['POTENCIA'])
                                            $lamp['POTENCIA'] = '';
                                    }

                                    $in_db = db_fn_query("SELECT COUNT(*) AS total FROM mobile_lamps AS lamps WHERE lamps.id = " . $lamp['NUM_SERIE_ID'])->fetch_object()->total;
                                    if ($in_db > 0) {
                                        // In database, update
                                        db_fn_query("UPDATE mobile_lamps SET serial='" . $lamp['NUMERO_SERIE'] . "', info='" . $lamp['ARTICULO'] . "', type='" . $lamp['TIPO'] . "', watts='" . $lamp['POTENCIA'] . "', active=1, cuadrilla=" . $lamp['ALM_CLAVE'] . " 
                                    WHERE id=" . $lamp['NUM_SERIE_ID']);
                                    } else {
                                        // Not in database, insert
                                        if ($lamp['NUM_SERIE_ID'] > LAMPS_LAST_ID_DEMO)
                                            $row_version = date("Y-m-d H:i:s");
                                        else
                                            $row_version = date("Y-m-d H:i:s", 1);
                                        db_fn_query("INSERT INTO mobile_lamps (id, serial, info, type, watts, installed, active, cuadrilla, row_version)
                                              VALUES (" . $lamp['NUM_SERIE_ID'] . ", '" . $lamp['NUMERO_SERIE'] . "', '" . $lamp['ARTICULO'] . "', '" . $lamp['TIPO'] . "', '" . $lamp['POTENCIA'] . "', 0, 1, " . $lamp['ALM_CLAVE'] . ", '" . $row_version . "')");
                                    }
                                }
                            }
                        }

                        if (count($SIILInfo['cuadrillas']) > 0 || count($SIILInfo['lamps']) > 0) {
                            $requestedBy = "{device:'" . $_GET['device'] . "',uuid:'" . $_GET['uuid'] . "',ip:'" . $_SERVER['REMOTE_ADDR'] . "'}";
                            db_fn_query("INSERT INTO mobile_log_operations (module, log, request_by) VALUES ('Synchronize', '" . count($SIILInfo['cuadrillas']) . " groups & " . count($SIILInfo['lamps']) . " lamps synchronized from SIIL information to local database', '" . addslashes($requestedBy) . "')");
                        }

                        header("HTTP/1.1 200 OK", true, 200);
                        echo json_encode(array('message' => 'Groups: ' . count($SIILInfo['cuadrillas']) . ', Lamps: ' . count($SIILInfo['lamps']) . '. Update OK'));
                        break;
                    default:
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        break;
                }
            }
            break;
        case "PUT":
        case "POST":
        case "DELETE":
        default:
            header("HTTP/1.0 405 Method Not Allowed", true, 405);
            die();
            break;
    }

    function getSIILInfo($previous_info)
    {
        $response = array('cuadrillas' => [], 'lamps' => []);

        /* CUADRILLAS */
        $date_cuadrillas = date('d-M-Y H:i:s', strtotime($previous_info['cuadrillas']['rv']));
        $db_result = oracle_query("SELECT * FROM almacenes WHERE tipo_unidad='S' AND (fecha_ult_act>TO_DATE('" . $date_cuadrillas . "', 'dd-mon-yyyy HH24:MI:SS') OR fecha_ult_act IS NULL)");
        if (!$db_result) {
            header("HTTP/1.0 400 Bad Request", true, 400);
            echo json_encode(array('message' => 'Error en consulta hacia al SIIL.'));
            exit;
        }
        $num_results = oci_fetch_all($db_result, $tmp);
        $update_needed = ($num_results > 0 && $num_results != count($previous_info['cuadrillas']['groups']));

        if (!$update_needed) {
            $db_result = oracle_query("SELECT * FROM almacenes WHERE tipo_unidad='S' AND (fecha_ult_act<=TO_DATE('" . $date_cuadrillas . "', 'dd-mon-yyyy HH24:MI:SS') OR fecha_ult_act IS NULL)");
            if (!$db_result) {
                header("HTTP/1.0 400 Bad Request", true, 400);
                echo json_encode(array('message' => 'Error en consulta hacia al SIIL.'));
                exit;
            }
            $num_results = oci_fetch_all($db_result, $tmp);
            $update_needed = ($num_results != count($previous_info['cuadrillas']['groups']));
        }

        if ($update_needed) {
            $db_result = oracle_query("SELECT clave, descripcion, alm_clave, cd_clave, edo_clave, pais_clave FROM almacenes WHERE tipo_unidad='S'");
            if ($db_result) {
                while ($row = oci_fetch_assoc($db_result)) {
                    $row['CLAVE'] = intval($row['CLAVE']);
                    $row['ALM_CLAVE'] = intval($row['ALM_CLAVE']);
                    $row['CD_CLAVE'] = intval($row['CD_CLAVE']);
                    $row['EDO_CLAVE'] = intval($row['EDO_CLAVE']);
                    $row['PAIS_CLAVE'] = intval($row['PAIS_CLAVE']);

                    array_push($response['cuadrillas'], $row);
                }
            } else {
                header("HTTP/1.0 400 Bad Request", true, 400);
                echo json_encode(array('message' => 'Error en consulta hacia al SIIL.'));
                exit;
            }
        }

        /* LAMPARAS */
        $cuadrillas = count($response['cuadrillas']) > 0 ? $response['cuadrillas'] : $previous_info['cuadrillas']['groups'];
        $date_lamps = date('d-M-Y H:i:s', strtotime($previous_info['lamps']['rv']));
        $condition = "";
        $variables = [];

        foreach ($cuadrillas as $index => $cuadrilla) {
            if (!!$cuadrilla['id'])
                $cuadrilla['CLAVE'] = $cuadrilla['id'];

            if ($condition != '')
                $condition .= " OR ";
            $condition .= "alm.clave = :p_cuadrilla_" . $index;
            $variables[':p_cuadrilla_' . $index] = $cuadrilla['CLAVE'];
        }

        $db_result = oracle_query("SELECT ns.numero_serie FROM numeros_serie ns, almacenes alm, articulos art 
                                  WHERE ns.exi_lo_al_alm_clave = alm.clave 
                                      AND ns.exi_lo_al_det_lote_art_clave = art.clave 
                                      AND ns.existencia = 'S' 
                                      AND ( ns.circuito_pos IS NULL OR ns.latitud IS NULL OR ns.longitud IS NULL )
                                      AND alm.tipo_unidad = 'S'
                                      AND (" . $condition . ")
                                      AND ns.fecha_alta>TO_DATE('" . $date_lamps . "', 'dd-mon-yyyy HH24:MI:SS')
                                      AND ns.id>" . LAMPS_LAST_ID_DEMO, $variables);
        if (!$db_result) {
            header("HTTP/1.0 400 Bad Request", true, 400);
            echo json_encode(array('message' => 'Error en consulta hacia al SIIL.'));
            exit;
        }
        $num_results = oci_fetch_all($db_result, $tmp);
        $update_needed = ($num_results > 0);

        if (!$update_needed) {
            $db_result = oracle_query("SELECT ns.numero_serie FROM numeros_serie ns, almacenes alm, articulos art 
                                      WHERE ns.exi_lo_al_alm_clave = alm.clave 
                                          AND ns.exi_lo_al_det_lote_art_clave = art.clave 
                                          AND ns.existencia = 'S' 
                                          AND ( ns.circuito_pos IS NULL OR ns.latitud IS NULL OR ns.longitud IS NULL )
                                          AND alm.tipo_unidad = 'S'
                                          AND (" . $condition . ")
                                          AND ns.fecha_alta<=TO_DATE('" . $date_lamps . "', 'dd-mon-yyyy HH24:MI:SS')
                                          AND ns.id>" . LAMPS_LAST_ID_DEMO, $variables);
            if (!$db_result) {
                header("HTTP/1.0 400 Bad Request", true, 400);
                echo json_encode(array('message' => 'Error en consulta hacia al SIIL.'));
                exit;
            }
            $num_results = oci_fetch_all($db_result, $tmp);
            $update_needed = ($num_results != $previous_info['lamps']['qty']);
        }

        if ($condition != '' && $update_needed) {
            $db_result = oracle_query("SELECT alm.clave alm_clave, alm.descripcion almacen, art.clave art_clave, art.descripcion articulo, ns.id num_serie_id, ns.numero_serie
                                      FROM numeros_serie ns, almacenes alm, articulos art 
                                      WHERE ns.exi_lo_al_alm_clave = alm.clave 
                                          AND ns.exi_lo_al_det_lote_art_clave = art.clave 
                                          AND ns.existencia = 'S' 
                                          AND ( ns.circuito_pos IS NULL OR ns.latitud IS NULL OR ns.longitud IS NULL )
                                          AND alm.tipo_unidad = 'S'
                                          AND (" . $condition . ")
                                          AND ns.id>" . LAMPS_LAST_ID_DEMO, $variables);
            if ($db_result) {
                while ($row = oci_fetch_assoc($db_result)) {
                    array_push($response['lamps'], $row);
                }
            } else {
                header("HTTP/1.0 400 Bad Request", true, 400);
                echo json_encode(array('message' => 'Error en consulta hacia al SIIL.'));
                exit;
            }
        }

        return $response;
    }

    if ($_SESSION['db_oracle'] != false) {
        // Close the Oracle connection
        oci_close($_SESSION['db_oracle']);
    }
?>