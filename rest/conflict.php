<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';

    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET, POST');
            break;
        case "GET":
            if (isset($_GET['action'])) {
                // Process Action
                switch ($_GET['action']) {
                    case "all":
                        $response = [];
                        // Get Request's row version
                        $requestRowVersion = 0;
                        $latestRowVersion = "";
                        if (isset($_GET['row_version'])) {
                            $requestRowVersion = date("Y-m-d H:i:s", $_GET['row_version']);
                        }
                        allConflicts($response, $requestRowVersion, $latestRowVersion);

                        header('Access-Control-Allow-Origin: *');
                        echo json_encode($response);
                        break;
                    default:
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        break;
                }
            }
            break;
        case "POST":
            header('Access-Control-Allow-Origin: *');
            $data = json_decode(file_get_contents('php://input'), true);
            if ($data['move'] != '' && $data['json'] != '') {
                saveConflict($data);
            } else {
                header("HTTP/1.0 400 Bad Request", true, 400);
                //echo 'Information must be completed';
                echo json_encode(array('message' => 'La información esta incompleta.'));
            }
            break;
        case "PUT":
        case "DELETE":
        default:
            header("HTTP/1.0 405 Method Not Allowed", true, 405);
            die();
            break;
    }

    function allConflicts(&$response, $requestRowVersion, &$latestRowVersion)
    {
        // Select all moves with created/modified date grather than row_version
        $db_result = db_fn_query("SELECT * FROM mobile_conflicts 
                                 WHERE row_version > '" . $requestRowVersion . "'");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // Look for newest row version
                $latestRowVersion = max($db_row['row_version'], $latestRowVersion);
                // unset row versions and unused id's so they don't appear in response
                //unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }

        // Convert response to json and echo it
        return $response;
    }

    function saveConflict($data)
    {
        // Save/Insert installation register
        if (db_fn_query("INSERT INTO mobile_conflicts (move, json, message) VALUES ('" . $data['move'] . "', '" . $data['json'] . "', '" . $data['message'] . "')")) {
            echo json_encode(array('message' => 'Instalación registrada.'));
        }
    }
?>