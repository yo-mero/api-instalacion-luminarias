<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';

    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET, PUT');
            break;
        case "GET":
            if (isset($_GET['action'])) {
                // Process Action
                switch ($_GET['action']) {
                    case "all":
                        $response = [];
                        // Get Request's row version
                        $requestRowVersion = 0;
                        $latestRowVersion = "";
                        if (isset($_GET['row_version']) && is_numeric($_GET['row_version']))
                            $requestRowVersion = date("Y-m-d H:i:s", $_GET['row_version']);
                        allPoles($response, $requestRowVersion, $latestRowVersion);

                        header('Access-Control-Allow-Origin: *');
                        echo json_encode($response);
                        break;
                    case "filter":
                        header('Access-Control-Allow-Origin: *');
                        $filter = '';
                        if (isset($_GET['field']) && isset($_GET['value'])) {
                            $filter = $_GET['field'] . "=" . $_GET['value'];
                            filterPoles($filter);
                        } else if (isset($_GET['move'])) {
                            filterPoles($filter);
                        } else {
                            header("HTTP/1.0 400 Bad Request", true, 400);
                        }
                        break;
                    default:
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        break;
                }
            }
            break;
        case "PUT":
            header('Access-Control-Allow-Origin: *');
            $data = json_decode(file_get_contents('php://input'), true);
            if (count($data) == $_GET['rows_to_update']) {
                if ($_GET['rows_to_update'] > 0) {
                    if (!updatePoles($data)) {
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        echo json_encode(array('message' => 'Hubo un error al actualizar las filas.'));
                    } else
                        echo json_encode(array('message' => 'Posición actualizada.'));
                }
            } else {
                header("HTTP/1.0 400 Bad Request", true, 400);
                echo json_encode(array('message' => 'La longitud de información no coincide con el número de filas a actualizar.'));
            }
            break;
        case "POST":
        case "DELETE":
        default:
            header("HTTP/1.0 405 Method Not Allowed", true, 405);
            die();
            break;
    }

    function allPoles(&$response, $requestRowVersion, &$latestRowVersion)
    {
        $lastID = isset($_GET['lastID']) ? $_GET['lastID'] : 0;
        /*if (isset($_GET['local_poles_numrows']) && intval($_GET['local_poles_numrows']) > 0) {
            $total_in_db = db_fn_query("SELECT COUNT(*) AS total FROM mobile_poles WHERE 1")->fetch_object()->total;
            if (intval($total_in_db) != intval($_GET['local_poles_numrows']))
                $requestRowVersion = date("Y-m-d H:i:s", 0);
        }*/
        // Select all moves with created/modified date grather than row_version
        $AND_OR = "OR";
        if ($requestRowVersion == date("Y-m-d H:i:s", 0))
            $AND_OR = "AND";
        $db_result = db_fn_query("SELECT * FROM mobile_poles 
                                 WHERE row_version > '" . $requestRowVersion . "' " . $AND_OR . " id > " . $lastID . "  
                                 ORDER BY id ASC");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // Look for newest row version
                $latestRowVersion = max($db_row['row_version'], $latestRowVersion);
                // unset row versions and unused id's so they don't appear in response
                //unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }

        // Convert response to json and echo it
        return $response;
    }

    function filterPoles($filter)
    {
        // Get poles filtered
        // If isset move, the result will be filter with the list of poles on moves table
        if (isset($_GET['move']) && intval($_GET['move']) > 0) {
            $list_poles = db_fn_query("SELECT moves.list_poles AS list FROM mobile_moves AS moves
                                        WHERE moves.id=" . $_GET['move'])->fetch_object()->list;

            $arrayList = explode(',', $list_poles);
            for ($i = 0; $i < count($arrayList); $i++)
                $arrayList[$i] = trim($arrayList[$i]);
            $list_poles = implode(',', $arrayList);

            $db_result = db_fn_query("SELECT poles.* FROM mobile_poles AS poles
                                      WHERE FIND_IN_SET(poles.id,'" . $list_poles . "')>0");
        } else
            $db_result = db_fn_query("SELECT poles.* FROM mobile_poles AS poles
                                        WHERE poles." . $filter);
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                if (isset($_GET['noInstalled']) && $_GET['noInstalled'] === true) {
                    $installed = !!db_fn_query("SELECT COUNT(*) AS total FROM mobile_installation AS i 
                                                    WHERE i.id_pole=" . $db_row['id'] . "
                                                    AND i.id_move=" . $_GET['move'])->fetch_object()->total;
                    if ($installed)
                        continue;
                }
                // unset row versions and unused id's so they don't appear in response
                unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);
                // Push result to response
                array_push($response, $db_row);
            }
        }
        // Convert response to json and echo it
        echo json_encode($response);
    }

    function updatePoles($data)
    {
        $result = true;
        for ($i = 0; $i < count($data); $i++) {
            if (is_numeric($data[$i]['id']) && $data[$i]['id'] > 0) {
                if (isset($data[$i]['potency']) && !isset($data[$i]['watts']))
                    $data[$i]['watts'] = $data[$i]['potency'];
                $db_result = db_fn_query("UPDATE mobile_poles SET 
                                          name='" . $data[$i]['name'] . "', 
                                          latitude=" . floatval($data[$i]['latitude']) . ",  
                                          longitude=" . floatval($data[$i]['longitude']) . ", 
                                          circuit='" . $data[$i]['circuit'] . "', 
                                          watts='" . $data[$i]['watts'] . "', 
                                          num_lamps=" . intval($data[$i]['num_lamps']) . ", 
                                          linked='" . $data[$i]['linked'] . "', 
                                          installed=" . intval($data[$i]['installed']) . ",
                                          row_version='" . date("Y-m-d H:i:s") . "' 
                                          WHERE id=" . $data[$i]['id']);
                if (!$db_result) {
                    $result = false;
                }
            } else
                $result = false;
        }
        return $result;
    }
?>