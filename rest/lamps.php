<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';

    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET, PUT');
            break;
        case "GET":
            if (isset($_GET['action'])) {
                // Process Action
                switch ($_GET['action']) {
                    case "all":
                        $response = [];
                        // Get Request's row version
                        $requestRowVersion = 0;
                        $latestRowVersion = "";
                        if (isset($_GET['row_version']) && is_numeric($_GET['row_version']))
                            $requestRowVersion = date("Y-m-d H:i:s", $_GET['row_version']);
                        allLamps($response, $requestRowVersion, $latestRowVersion);

                        header('Access-Control-Allow-Origin: *');
                        echo json_encode($response);
                        break;
                    case "mobile":
                        $response = [];
                        // Get Request's row version
                        $requestRowVersion = 0;
                        $latestRowVersion = "";
                        if (isset($_GET['row_version']) && is_numeric($_GET['row_version']))
                            $requestRowVersion = date("Y-m-d H:i:s", $_GET['row_version']);
                        allLampsMobile($response, $requestRowVersion, $latestRowVersion);

                        header('Access-Control-Allow-Origin: *');
                        echo json_encode($response);
                        break;
                    case "filter":
                        header('Access-Control-Allow-Origin: *');
                        $filter = '';
                        if (isset($_GET['field']) && isset($_GET['value'])) {
                            $filter = $_GET['field'] . "=" . $_GET['value'];
                            filterPoles($filter);
                        } else {
                            header("HTTP/1.0 400 Bad Request", true, 400);
                        }
                        break;
                    default:
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        break;
                }
            }
            break;
        case "PUT":
            header('Access-Control-Allow-Origin: *');
            $data = json_decode(file_get_contents('php://input'), true);
            if (count($data) == $_GET['rows_to_update']) {
                if ($_GET['rows_to_update'] > 0) {
                    if (!updateLamps($data)) {
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        echo json_encode(array('message' => 'Hubo un error al actualizar las filas.'));
                    } else
                        echo json_encode(array('message' => 'Lampara actualizada.'));
                }
            } else {
                header("HTTP/1.0 400 Bad Request", true, 400);
                echo json_encode(array('message' => 'La longitud de información no coincide con el número de filas a actualizar.'));
            }
            break;
        case "POST":
        case "DELETE":
        default:
            header("HTTP/1.0 405 Method Not Allowed", true, 405);
            die();
            break;
    }

    function allLamps(&$response, $requestRowVersion, &$latestRowVersion)
    {
        $lastID = isset($_GET['lastID']) ? $_GET['lastID'] : 0;
        /*if (isset($_GET['local_lamps_numrows']) && intval($_GET['local_lamps_numrows']) > 0) {
            $total_in_db = db_fn_query("SELECT COUNT(*) AS total FROM mobile_lamps WHERE active=1 AND (row_version > '" . date("Y-m-d H:i:s", 0) . "')")->fetch_object()->total;
            if (intval($total_in_db) != intval($_GET['local_lamps_numrows']))
                $requestRowVersion = date("Y-m-d H:i:s", 0);
        }*/
        // Select all moves with created/modified date grather than row_version
        $AND_OR = "OR";
        if ($requestRowVersion == date("Y-m-d H:i:s", 0))
            $AND_OR = "AND";
        $db_result = db_fn_query("SELECT * FROM mobile_lamps 
                                 WHERE active=1 AND (row_version > '" . $requestRowVersion . "' " . $AND_OR . " id > " . $lastID . ")   
                                 ORDER BY id ASC");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // Look for newest row version
                $latestRowVersion = max($db_row['row_version'], $latestRowVersion);
                // unset row versions and unused id's so they don't appear in response
                //unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }

        // Convert response to json and echo it
        return $response;
    }

    function allLampsMobile(&$response, $requestRowVersion, &$latestRowVersion)
    {
        $lastID = isset($_GET['lastID']) ? $_GET['lastID'] : 0;
        // Select all moves with created/modified date grather than row_version
        $AND_OR = "OR";
        if ($requestRowVersion == date("Y-m-d H:i:s", 0))
            $AND_OR = "AND";
        $db_result = db_fn_query("SELECT * FROM mobile_lamps 
                                     WHERE (
                                            (active=1 AND row_version>'" . $requestRowVersion . "') 
                                            OR (active=0 AND row_version>'" . date("Y-m-d 00:00:00", strtotime('-7 days')) . "')
                                        ) " . $AND_OR . " id>" . $lastID . "   
                                     ORDER BY id ASC");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // Look for newest row version
                $latestRowVersion = max($db_row['row_version'], $latestRowVersion);
                // unset row versions and unused id's so they don't appear in response
                //unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }

        // Convert response to json and echo it
        return $response;
    }

    function filterLamps($filter)
    {
        // Get lamps filtered
        // If isset move, the result will be filter with the list of lamps on moves table
        if (isset($_GET['move']) && intval($_GET['move']) > 0) {
            $list_lamps = db_fn_query("SELECT moves.list_lamps AS list FROM mobile_moves AS moves
                                        WHERE moves.id=" . $_GET['move'])->fetch_object()->list;

            $arrayList = explode(',', $list_lamps);
            for ($i = 0; $i < count($arrayList); $i++)
                $arrayList[$i] = trim($arrayList[$i]);
            $list_lamps = implode(',', $arrayList);

            $installedCondition = '';
            if (isset($_GET['no_installed']) && $_GET['no_installed'] == true) {
                $installedCondition = " AND NOT EXISTS (SELECT * FROM installation AS i WHERE FIND_IN_SET(lamps.id, i.id_lamp)>0)";
            }

            $db_result = db_fn_query("SELECT lamps.* FROM mobile_lamps AS lamps
                                      WHERE active=1 AND (FIND_IN_SET(lamps.id,'" . $list_lamps . "')>0" . $installedCondition . ")");
        } else
            $db_result = db_fn_query("SELECT lamps.* FROM mobile_lamps AS lamps
                                        WHERE active=1 AND (lamps." . $filter . ")");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // unset row versions and unused id's so they don't appear in response
                unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }
        // Convert response to json and echo it
        echo json_encode($response);
    }

    function updateLamps($data)
    {
        $result = true;
        for ($i = 0; $i < count($data); $i++) {
            if (is_numeric($data[$i]['id']) && $data[$i]['id'] > 0) {
                $db_result = db_fn_query("UPDATE mobile_lamps SET 
                                              serial='" . $data[$i]['serial'] . "', 
                                              info='" . $data[$i]['info'] . "',  
                                              type='" . $data[$i]['type'] . "', 
                                              watts='" . $data[$i]['watts'] . "', 
                                              installed=" . intval($data[$i]['installed']) . ", 
                                              active=1, 
                                              cuadrilla=" . intval($data[$i]['cuadrilla']) . ", 
                                              row_version='" . date("Y-m-d H:i:s") . "' 
                                              WHERE id=" . $data[$i]['id']);
                if (!$db_result) {
                    $result = false;
                }
            } else
                $result = false;
        }
        return $result;
    }
?>