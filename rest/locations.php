<?php
/**
 * Created by IntelliJ IDEA.
 * User: gcuellar
 * Date: 13/12/17
 * Time: 04:33 PM
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';

function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function sendError($message = '', $error = [])
{
    if (!$error['msg'] || !$error['code'])
        $error = array('msg' => 'Bad Request', 'code' => 400);
    header("HTTP/1.0 " . $error['code'] . " " . $error['msg'], true, $error['code']);
    if (!!$message)
        echo json_encode(array('message' => $message));
    exit($error['code']);
}

switch ($_SERVER['REQUEST_METHOD']) {
    case "OPTIONS":
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST');
        break;
    case "GET":
        header('Access-Control-Allow-Origin: http://' . $_SERVER['HTTP_HOST']);
        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'types':
                    $translateOfApps = array(
                        "Installations" => "Instalaciones",
                        "Liftings" => "Levantamientos"
                    );
                    $result = db_fn_query("SELECT DISTINCT app FROM device_locations ORDER BY app");
                    if (method_exists('mysqli_result', 'fecth_all'))
                        $response = $result->fecth_all(MYSQLI_ASSOC);
                    else
                        for ($response = array(); $tmp = $result->fetch_assoc();) $response[] = $tmp;
                    foreach ($response as $key=>$app) {
                        $response[$key]['text'] = $translateOfApps[$app['app']];
                    }
                    echo json_encode($response);
                    break;
                case 'list':
                    if (isset($_GET['startDate']) && isset($_GET['endDate']) && isset($_GET['app'])) {
                        $startDate = date('Y-m-d 00:00:00', $_GET['startDate']);
                        $endDate = date('Y-m-d 23:59:59', $_GET['endDate']);
                        if (validateDate($startDate) && validateDate($endDate) && $_GET['app'] != '') {
                            // Getting devices
                            $devices = array();
                            $result = db_fn_query("SELECT DISTINCT device FROM device_locations WHERE app='" . $_GET['app'] . "' AND datetime BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY device");
                            if (method_exists('mysqli_result', 'fecth_all'))
                                $devices = $result->fecth_all(MYSQLI_ASSOC);
                            else
                                for (; $tmp = $result->fetch_assoc();) $devices[] = $tmp;

                            // Getting data
                            foreach ($devices as $key => $device) {
                                $devices[$key] = json_decode($device['device'], true);
                                $devices[$key]['locations'] = array();
                                $result = db_fn_query("SELECT datetime, coords FROM device_locations WHERE app='" . $_GET['app'] . "' AND device='" . $device['device'] . "' AND datetime BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY datetime");
                                if (method_exists('mysqli_result', 'fecth_all'))
                                    $devices[$key]['locations'] = $result->fecth_all(MYSQLI_ASSOC);
                                else
                                    for (; $tmp = $result->fetch_assoc();) $devices[$key]['locations'][] = $tmp;
                            }

                            echo json_encode($devices);
                        } else
                            sendError('Verifique que las fechas sean validas.');
                    } else
                        sendError();
                    break;
                default:
                    sendError();
            }
        } else
            sendError();
        break;
    case "POST":
        header('Access-Control-Allow-Origin: *');
        save(json_decode(file_get_contents('php://input'), true));
        break;
    case "PUT":
    case "DELETE":
    default:
        header("HTTP/1.0 405 Method Not Allowed", true, 405);
        die();
        break;
}

function save($data)
{
    $allOk = true;

    $db_conn = $_SESSION['db_conn_main'];
    $db_conn->query('SET CHARACTER SET utf8');

    parse_str($data['device'], $device);
    $requestedBy = array(
        'device' => $device['device'],
        'uuid' => $device['uuid']
    );
    $app = $data['app'];
    $device = addslashes(json_encode($requestedBy));

    $insert = $db_conn->prepare("INSERT INTO device_locations (app, device, ip, datetime, coords) VALUES ('" . $app . "', '" . $device . "', '" . $_SERVER['REMOTE_ADDR'] ."', ?, ?)");
    $insert->bind_param("ss", $date, $coords);

    $count = $db_conn->prepare("SELECT COUNT(*) FROM device_locations WHERE app=? AND device=? AND datetime=?");
    $count->bind_Param("sss", $app, $device, $date);

    foreach ($data['results'] as $location) {
        if (validateDate($location['date']) && $location['coords'] != '') {

            $result = db_fn_query("SELECT COUNT(*) as total FROM device_locations WHERE app='" . $app . "' AND device='" . $device . "' AND datetime='" . $location['date'] . "'")->fetch_object()->total;
            if (intval($result) == 0) {
                $date = $location['date'];
                $coords = $location['coords'];
                if (!$insert->execute())
                    $allOk = false;
            }
        } else
            $allOk = false;
    }

    if (!$allOk)
        sendError();
}

?>