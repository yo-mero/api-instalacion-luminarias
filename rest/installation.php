<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';

    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET, POST');
            break;
        case "GET":
            if (isset($_GET['action'])) {
                // Process Action
                if (isset($_GET['device']) && $_GET['row_version'] == 0) {
                    $requestedBy = "{device:'" . $_GET['device'] . "',uuid:'" . $_GET['uuid'] . "',ip:'" . $_SERVER['REMOTE_ADDR'] . "'}";
                    db_fn_query("INSERT INTO mobile_log_operations (module, log, request_by) VALUES ('Installation', 'First request from a new/cleaned device, it retrieves the whole information for the app to work', '" . addslashes($requestedBy) . "')");
                }
                switch ($_GET['action']) {
                    case "all":
                        $response = [];
                        // Get Request's row version
                        $requestRowVersion = 0;
                        $latestRowVersion = "";
                        if (isset($_GET['row_version']) && is_numeric($_GET['row_version']))
                            $requestRowVersion = date("Y-m-d H:i:s", $_GET['row_version']);
                        allInstallations($response, $requestRowVersion, $latestRowVersion);

                        header('Access-Control-Allow-Origin: *');
                        echo json_encode($response);
                        break;
                    case "filter":
                        if (isset($_GET['field']) && isset($_GET['value'])) {
                            $filter = $_GET['field'] . "=" . $_GET['value'];
                            header('Access-Control-Allow-Origin: *');
                            filterInstallations($filter);
                        } else {
                            header("HTTP/1.0 400 Bad Request", true, 400);
                        }
                        break;
                    default:
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        break;
                }
            }
            break;
        case "POST":
            header('Access-Control-Allow-Origin: *');
            $data = json_decode(file_get_contents('php://input'), true);
            if (is_numeric($data['id_move']) && is_numeric($data['id_pole']) && !!$data['id_lamp']) {
                saveInstallation($data);
            } else {
                header("HTTP/1.0 400 Bad Request", true, 400);
                //echo 'Information must be completed';
                echo json_encode(array('message' => 'La información esta incompleta.'));
            }
            break;
        case "PUT":
        case "DELETE":
        default:
            header("HTTP/1.0 405 Method Not Allowed", true, 405);
            die();
            break;
    }

    function allInstallations(&$response, $requestRowVersion, &$latestRowVersion)
    {
        /*if (isset($_GET['local_install_numrows']) && intval($_GET['local_install_numrows']) > 0) {
            $total_in_db = db_fn_query("SELECT COUNT(*) AS total FROM mobile_installation WHERE 1")->fetch_object()->total;
            if ($total_in_db != intval($_GET['local_install_numrows']))
                $requestRowVersion = date("Y-m-d H:i:s", 0);
        }*/
        // Select all moves with created/modified date grather than row_version
        $db_result = db_fn_query("SELECT * FROM mobile_installation 
                                 WHERE row_version > '" . $requestRowVersion . "'");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // Look for newest row version
                $latestRowVersion = max($db_row['row_version'], $latestRowVersion);
                // unset row versions and unused id's so they don't appear in response
                unset($db_row['ended']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);
                $db_row['photoBefore'] = '';
                $db_row['photoAfter'] = '';

                // Push result to response
                array_push($response, $db_row);
            }
        }

        // Convert response to json and echo it
        return $response;
    }

    function filterInstallations($filter)
    {
        // Get lamps filtered
        // If isset move, the result will be filter with the list of lamps on moves table
        $db_result = db_fn_query("SELECT i.* FROM mobile_installation AS i
                                  WHERE i." . $filter);
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // unset row versions and unused id's so they don't appear in response
                unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }
        // Convert response to json and echo it
        echo json_encode($response);
    }

    function saveInstallation($data)
    {
        if (isset($data['id']) && intval($data['id']) > 0)
            $filterCondition = "id=" . intval($data['id']);
        else
            $filterCondition = "id_move=" . $data['id_move'] . " AND id_pole=" . $data['id_pole'];

        if (!db_fn_query("SELECT COUNT(*) AS total FROM mobile_installation WHERE ended=0 AND " . $filterCondition)->fetch_object()->total) {
            // Save/Insert installation register
            if (db_fn_query("INSERT INTO mobile_installation (id_move, id_pole, id_lamp, installed_date, photoBefore, photoAfter, rmv_type, rmv_power, rmv_bal, address, addr_number, street1, street2, comments, gps_coords, linked, row_version)
                  VALUES (" . $data['id_move'] . ", " . $data['id_pole'] . ", " . $data['id_lamp'] . ", '" . $data['installed_date'] . "', '" . $data['photoBefore'] . "', '" . $data['photoAfter'] . "', '" . $data['rmv_type'] . "', '" . $data['rmv_power'] . "', '" . $data['rmv_bal'] . "', '" . $data['address'] . "', '" . $data['addr_number'] . "', '" . $data['street1'] . "', '" . $data['street2'] . "', '" . $data['comments'] . "', '" . $data['gps_coords'] . "', " . $data['linked'] . ", '" . date("Y-m-d H:i:s") . "')")) {
                echo json_encode(array('message' => 'Instalación registrada.'));
            } else {
                header("HTTP/1.0 400 Bad Request", true, 400);
            }
        } else {
            // Update installation register
            if (isset($data['id']) && intval($data['id']) > 0) {
                if ($data['photoBefore'] != '')
                    $query = "UPDATE mobile_installation SET id_pole=" . $data['id_pole'] . ", id_lamp=" . $data['id_lamp'] . ", installed_date='" . $data['installed_date'] . "', photoBefore='" . $data['photoBefore'] . "', photoAfter='" . $data['photoAfter'] . "', rmv_type='" . $data['rmv_type'] . "', rmv_power='" . $data['rmv_power'] . "', rmv_bal='" . $data['rmv_bal'] . "', address='" . $data['address'] . "', addr_number='" . $data['addr_number'] . "', street1='" . $data['street1'] . "', street2='" . $data['street2'] . "', comments='" . $data['comments'] . "', gps_coords='" . $data['gps_coords'] . "', row_version='" . date("Y-m-d H:i:s") . "' WHERE id=" . $data['id'];
                else
                    $query = "UPDATE mobile_installation SET id_pole=" . $data['id_pole'] . ", id_lamp=" . $data['id_lamp'] . ", installed_date='" . $data['installed_date'] . "', rmv_type='" . $data['rmv_type'] . "', rmv_power='" . $data['rmv_power'] . "', rmv_bal='" . $data['rmv_bal'] . "', address='" . $data['address'] . "', addr_number='" . $data['addr_number'] . "', street1='" . $data['street1'] . "', street2='" . $data['street2'] . "', comments='" . $data['comments'] . "', gps_coords='" . $data['gps_coords'] . "', row_version='" . date("Y-m-d H:i:s") . "' WHERE id=" . $data['id'];
                if (db_fn_query($query)) {
                    echo json_encode(array('message' => 'Instalación actualizada.'));
                } else {
                    header("HTTP/1.0 400 Bad Request", true, 400);
                }
            } else {
                if ($data['photoBefore'] != '')
                    $query = "UPDATE mobile_installation SET id_lamp=" . $data['id_lamp'] . ", installed_date='" . $data['installed_date'] . "', photoBefore='" . $data['photoBefore'] . "', photoAfter='" . $data['photoAfter'] . "', rmv_type='" . $data['rmv_type'] . "', rmv_power='" . $data['rmv_power'] . "', rmv_bal='" . $data['rmv_bal'] . "', address='" . $data['address'] . "', addr_number='" . $data['addr_number'] . "', street1='" . $data['street1'] . "', street2='" . $data['street2'] . "', comments='" . $data['comments'] . "', gps_coords='" . $data['gps_coords'] . "', row_version='" . date("Y-m-d H:i:s") . "' WHERE id_move=" . $data['id_move'] . " AND id_pole=" . $data['id_pole'];
                else
                    $query = "UPDATE mobile_installation SET id_lamp=" . $data['id_lamp'] . ", installed_date='" . $data['installed_date'] . "', rmv_type='" . $data['rmv_type'] . "', rmv_power='" . $data['rmv_power'] . "', rmv_bal='" . $data['rmv_bal'] . "', address='" . $data['address'] . "', addr_number='" . $data['addr_number'] . "', street1='" . $data['street1'] . "', street2='" . $data['street2'] . "', comments='" . $data['comments'] . "', gps_coords='" . $data['gps_coords'] . "', row_version='" . date("Y-m-d H:i:s") . "' WHERE id_move=" . $data['id_move'] . " AND id_pole=" . $data['id_pole'];
                if (db_fn_query($query)) {
                    echo json_encode(array('message' => 'Instalación actualizada.'));
                } else {
                    header("HTTP/1.0 400 Bad Request", true, 400);
                }
            }
        }
        /*} else {
            header("HTTP/1.0 409 Conflict", true, 409);
            echo json_encode(array('message' => 'Lampara(s) ya registrada(s) como instalada(s) en este movimiento.'));
        }*/
    }
?>