<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/siil.inc';

    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET, PUT');
            break;
        case "GET":
            if (isset($_GET['action'])) {
                // Process Action
                switch ($_GET['action']) {
                    case "all":
                        $response = [];
                        // Get Request's row version
                        $requestRowVersion = 0;
                        $latestRowVersion = "";
                        if (isset($_GET['row_version']) && is_numeric($_GET['row_version']))
                            $requestRowVersion = date("Y-m-d H:i:s", $_GET['row_version']);
                        allGroups($response, $requestRowVersion, $latestRowVersion);

                        header('Access-Control-Allow-Origin: *');
                        echo json_encode($response);
                        break;
                    default:
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        break;
                }
            }
            break;
        case "PUT":
        case "POST":
        case "DELETE":
        default:
            header("HTTP/1.0 405 Method Not Allowed", true, 405);
            die();
            break;
    }

    function allGroups(&$response, $requestRowVersion, &$latestRowVersion)
    {
        // Select all moves with date less or same to current day and created/modified date greater than row_version
        $today = date("Y-m-d");

        if (isset($_GET['local_groups_numrows']) && intval($_GET['local_groups_numrows']) > 0) {
            $total_in_db = db_fn_query("SELECT COUNT(*) AS total FROM mobile_groups WHERE 1")->fetch_object()->total;
            if ($total_in_db != intval($_GET['local_groups_numrows']))
                $requestRowVersion = date("Y-m-d H:i:s", 0);
        }
        $db_result = db_fn_query("SELECT * FROM mobile_groups 
                                  WHERE row_version > '" . $requestRowVersion . "'");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // Look for newest row version
                $latestRowVersion = max($db_row['row_version'], $latestRowVersion);
                // unset row versions and unused id's so they don't appear in response
                //unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }

        // Convert response to json and echo it
        return json_encode($response);
    }

    function updateGroups($groups)
    {
        $result = true;
        for ($i = 0; $i < count($groups); $i++) {
            $db_result = db_fn_query("UPDATE mobile_groups SET 
                                      name='" . $groups[$i]['name'] . "', 
                                      alm_clave=" . intval($groups[$i]['alm_clave']) . ",  
                                      cd_clave=" . $groups[$i]['cd_clave'] . ", 
                                      edo_clave=" . $groups[$i]['edo_clave'] . ", 
                                      pais_clave=" . $groups[$i]['pais_clave'] . ",    
                                      row_version='" . date("Y-m-d H:i:s") . "' 
                                      WHERE id=" . $groups[$i]['id']);
            if (!$db_result) {
                $result = false;
            }
        }
        return $result;
    }
?>
