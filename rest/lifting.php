<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';

    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET, POST');
            break;
        case "GET":
            if (isset($_GET['action'])) {
                header('Access-Control-Allow-Origin: *');
                // Process Action
                if (isset($_GET['device']) && $_GET['row_version'] == 0) {
                    $requestedBy = "{device:'" . $_GET['device'] . "',uuid:'" . $_GET['uuid'] . "',ip:'" . $_SERVER['REMOTE_ADDR'] . "'}";
                    db_fn_query("INSERT INTO mobile_log_operations (module, log, request_by) VALUES ('Lifting', 'First request from a new/cleaned device, it retrieves the whole information', '" . addslashes($requestedBy) . "')");
                }
                switch ($_GET['action']) {
                    case "all":
                        $response = [];
                        // Get Request's row version
                        $requestRowVersion = 0;
                        $latestRowVersion = "";
                        if (isset($_GET['row_version']) && is_numeric($_GET['row_version']))
                            $requestRowVersion = date("Y-m-d H:i:s", $_GET['row_version']);
                        allLiftings($response, $requestRowVersion, $latestRowVersion);

                        echo json_encode($response);
                        break;
                    case "distinct":
                        $response = [];
                        distinctLiftings($response);
                        echo json_encode($response);
                        break;
                    default:
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        break;
                }
            }
            break;
        case "POST":
            header('Access-Control-Allow-Origin: *');
            $data = json_decode(file_get_contents('php://input'), true);
            if (is_numeric($data['id']) && validateDate($data['register_date']) && $data['coords'] != "") {
                saveLifting($data);
            } else {
                header("HTTP/1.0 400 Bad Request", true, 400);
                //echo 'Information must be completed';
                echo json_encode(array('message' => 'La información esta incompleta.'));
            }
            break;
        case "PUT":
        case "DELETE":
        default:
            header("HTTP/1.0 405 Method Not Allowed", true, 405);
            die();
            break;
    }

    function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    function allLiftings(&$response, $requestRowVersion, &$latestRowVersion)
    {
        // Select all moves with created/modified date grather than row_version
        $db_result = db_fn_query("SELECT * FROM mobile_lifting 
                                 WHERE row_version > '" . $requestRowVersion . "'");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // Look for newest row version
                $latestRowVersion = max($db_row['row_version'], $latestRowVersion);
                // unset row versions and unused id's so they don't appear in response

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }

        // Convert response to json and echo it
        return $response;
    }

    function distinctLiftings(&$response) {
        // Select all moves with created/modified date grather than row_version
        $db_result = db_fn_query("SELECT a.* FROM mobile_lifting a INNER JOIN 
                                (SELECT street_name, MAX(register_date) AS register_date FROM mobile_lifting GROUP BY street_name) AS b
                                ON a.street_name = b.street_name AND a.register_date = b.register_date");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // unset row versions and unused id's so they don't appear in response

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }

        return $response;
    }

    function saveLifting($data)
    {
        if (isset($data['id']) && is_numeric($data['id']) > 0) {
            if (!db_fn_query("SELECT COUNT(*) AS total FROM mobile_lifting WHERE coords='" . $data['coords'] . "' AND id!=" . $data['id'])->fetch_object()->total) {
                if (!db_fn_query("SELECT COUNT(*) AS total FROM mobile_lifting WHERE id=" . $data['id'])->fetch_object()->total) {
                    // Save/Insert installation register
                    if (db_fn_query("INSERT INTO mobile_lifting (id, register_date, coords, street_name, street_type, street_number_lanes, street_material, street_ridge, street_ridge_wide, street_ridge_size, street_light_availability, pole_type, pole_double_tension, pole_number_street_lights, wiring_mode, connection_mode, street_light_technology, street_light_application, street_light_height, street_light_arm, row_version)
                  VALUES (" . $data['id'] . ",'" . $data['register_date'] . "','" . $data['coords'] . "','" . $data['street_name'] . "','" . $data['street_type'] . "'," . $data['street_number_lanes'] . ",'" . $data['street_material'] . "'," . $data['street_ridge'] . "," . $data['street_ridge_wide'] . "," . $data['street_ridge_size'] . ",'" . $data['street_light_availability'] . "','" . $data['pole_type'] . "'," . $data['pole_double_tension'] . "," . $data['pole_number_street_lights'] . ",'" . $data['wiring_mode'] . "','" . $data['connection_mode'] . "','" . $data['street_light_technology'] . "','" . $data['street_light_application'] . "'," . $data['street_light_height'] . "," . $data['street_light_arm'] . ",'" . date("Y-m-d H:i:s") . "')")) {
                        echo json_encode(array('message' => 'Registrado.'));
                    } else {
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        echo json_encode(array('message' => 'Error al registrar.'));
                    }
                } else {
                    // Update installation register
                    if (db_fn_query("UPDATE mobile_lifting SET register_date='" . $data['register_date'] . "', coords='" . $data['coords'] . "', street_name='" . $data['street_name'] . "', street_type='" . $data['street_type'] . "', street_number_lanes=" . $data['street_number_lanes'] . ", street_material='" . $data['street_material'] . "', street_ridge=" . $data['street_ridge'] . ", street_ridge_wide=" . $data['street_ridge_wide'] . ", street_ridge_size=" . $data['street_ridge_size'] . ", street_light_availability='" . $data['street_light_availability'] . "', pole_type='" . $data['pole_type'] . "', pole_double_tension=" . $data['pole_double_tension'] . ", pole_number_street_lights=" . $data['pole_number_street_lights'] . ", wiring_mode='" . $data['wiring_mode'] . "', connection_mode='" . $data['connection_mode'] . "', street_light_technology='" . $data['street_light_technology'] . "', street_light_application='" . $data['street_light_application'] . "', street_light_height=" . $data['street_light_height'] . ", street_light_arm=" . $data['street_light_arm'] . ", row_version='" . date("Y-m-d H:i:s") . "' WHERE id=" . $data['id'])) {
                        echo json_encode(array('message' => 'Actualizado.'));
                    } else {
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        echo json_encode(array('message' => 'Error al actualizar.'));
                    }
                }
            } else {
                header("HTTP/1.0 409 Conflict", true, 409);
                echo json_encode(array('message' => 'Posición ya registrada.'));
            }
        } else
            header("HTTP/1.0 400 Bad Request", true, 400);
    }
?>