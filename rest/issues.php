<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';

    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET');
            break;
        case "GET":
            if (isset($_GET['action'])) {
                // Process Action
                switch ($_GET['action']) {
                    case "all":
                        $response = [];
                        // Get Request's row version
                        $requestRowVersion = 0;
                        $latestRowVersion = "";
                        if (isset($_GET['row_version']) && is_numeric($_GET['row_version']))
                            $requestRowVersion = date("Y-m-d H:i:s", $_GET['row_version']);
                        allIssues($response, $requestRowVersion, $latestRowVersion);

                        header('Access-Control-Allow-Origin: *');
                        echo json_encode($response);
                        break;
                    case "filter":
                        header('Access-Control-Allow-Origin: *');
                        $filter = '';
                        if (isset($_GET['field']) && isset($_GET['value'])) {
                            $filter = $_GET['field'] . "=" . $_GET['value'];
                            filterPoles($filter);
                        } else {
                            header("HTTP/1.0 400 Bad Request", true, 400);
                        }
                        break;
                    default:
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        break;
                }
            }
            break;
        case "PUT":
        case "POST":
        case "DELETE":
        default:
            header("HTTP/1.0 405 Method Not Allowed", true, 405);
            die();
            break;
    }

    function allIssues(&$response, $requestRowVersion, &$latestRowVersion)
    {
        $lastID = isset($_GET['lastID']) ? $_GET['lastID'] : 0;
        if (isset($_GET['local_numrows']) && intval($_GET['local_numrows']) > 0) {
            $total_in_db = db_fn_query("SELECT COUNT(*) AS total FROM mobile_issues WHERE 1")->fetch_object()->total;
            if (intval($total_in_db) != intval($_GET['local_numrows']))
                $requestRowVersion = date("Y-m-d H:i:s", 0);
        }
        // Select all moves with created/modified date grather than row_version
        $AND_OR = "OR";
        if ($requestRowVersion == date("Y-m-d H:i:s", 0))
            $AND_OR = "AND";
        $db_result = db_fn_query("SELECT * FROM mobile_issues
                                 WHERE row_version > '" . $requestRowVersion . "' " . $AND_OR . " id > " . $lastID . " 
                                 ORDER BY id ASC");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // Look for newest row version
                $latestRowVersion = max($db_row['row_version'], $latestRowVersion);
                // unset row versions and unused id's so they don't appear in response
                //unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }

        // Convert response to json and echo it
        return $response;
    }

    function filterPoles($filter)
    {
        // Get filtered
        $db_result = db_fn_query("SELECT * FROM mobile_issues
                                    WHERE " . $filter);
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // unset row versions and unused id's so they don't appear in response
                //unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);
                // Push result to response
                array_push($response, $db_row);
            }
        }
        // Convert response to json and echo it
        echo json_encode($response);
    }
?>