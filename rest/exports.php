<?php
/**
 * Created by IntelliJ IDEA.
 * User: gcuellar
 * Date: 19/10/17
 * Time: 04:45 PM
 */
function sendError($message = '', $error = [])
{
    if (!$error['msg'] || !$error['code'])
        $error = array('msg' => 'Bad Request', 'code' => 400);
    header("HTTP/1.0 " . $error['code'] . " " . $error['msg'], true, $error['code']);
    if (!!$message)
        echo json_encode(array('message' => $message));
    exit($error['code']);
}

if (strtolower(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST)) == strtolower($_SERVER['HTTP_HOST'])) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';

    function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    $old_files = db_fn_query("SELECT id, tmpfile FROM download_log WHERE downloaded=0 AND date<SUBDATE(NOW(),1)");
    if ($old_files) {
        while ($old_file = $old_files->fetch_assoc()) {
            if (db_fn_query("UPDATE download_log SET downloaded=1 WHERE id=" . $old_file['id']))
                unlink($old_file['tmpfile']);
        }
    }

    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            header('Access-Control-Allow-Origin:  http://' . $_SERVER['HTTP_HOST']);
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET, POST');
            break;
        case "GET":
            header('Access-Control-Allow-Origin: http://' . $_SERVER['HTTP_HOST']);
            if (isset($_GET['startDate']) && isset($_GET['endDate'])) {
                $startDate = date('Y-m-d 00:00:00', $_GET['startDate']);
                $endDate = date('Y-m-d 23:59:59', $_GET['endDate']);
                if (validateDate($startDate) && validateDate($endDate))
                    echo json_encode(array('registers' => db_fn_query("SELECT COUNT(*) AS total FROM mobile_installation WHERE installed_date BETWEEN  '" . $startDate . "' AND '" . $endDate . "'")->fetch_object()->total));
                else
                    sendError('Verifique que las fechas sean validas.');
            } else if (isset($_GET['file']) && $_GET['file'] !== '') {
                $fileInDB = db_fn_query("SELECT id, file AS name, downloaded FROM download_log WHERE ip='" . $_SERVER['REMOTE_ADDR'] . "' AND id_user=" . (isset($_GET['user']) ? intval($_GET['user']) : 0) . " AND tmpfile='" . $_GET['file'] . "' LIMIT 1");
                if (!$fileInDB)
                    sendError('', array('msg' => 'Not Found', 'code' => 404));

                $fileInDB = $fileInDB->fetch_object();
                if (!$fileInDB || $fileInDB->downloaded == 1)
                    sendError('El archivo ya fue descargado.', array('msg' => 'Gone', 'code' => 410));

                if (db_fn_query("UPDATE download_log SET date='" . date('Y-m-d H:i:s') . "', downloaded=1 WHERE id=" . $fileInDB->id)) {
                    header('Content-Type: application/force-download');
                    header('Content-Type: application/octet-stream');
                    header('Content-Type: application/download');
                    header('Content-Length: ' . filesize($_GET['file']));
                    header('Content-Disposition: attachment; filename="' . $fileInDB->name . '"');
                    readfile($_GET['file']);
                    unlink($_GET['file']);
                }
            } else
                sendError();
            break;
        case "POST":
            header('Access-Control-Allow-Origin: http://' . $_SERVER['HTTP_HOST']);
            if (count($_POST) > 0)
                $data = $_POST;
            else
                $data = json_decode(file_get_contents('php://input'), true);

            if (isset($data['startDate']) && isset($data['endDate']) && isset($data['download'])) {
                $startDate = date('Y-m-d 00:00:00', $data['startDate']);
                $endDate = date('Y-m-d 23:59:59', $data['endDate']);
                if (validateDate($startDate) && validateDate($endDate)) {
                    switch ($data['download']) {
                        case 'images':
                            $installations = db_fn_query("SELECT i.photo_before, i.photo_after, p.name AS posicion FROM mobile_installation i LEFT JOIN mobile_poles p ON p.id = i.id_position WHERE i.installed_date BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
                            if (!$installations)
                                sendError('', array('msg' => 'No Content', 'code' => 204));
                            $zipName = 'ImagenesInstalaciones_' . date('Ymd', $data['startDate']) . '-' . date('Ymd', $data['endDate']) . '.zip';
                            $zipFile = tempnam(sys_get_temp_dir(), "zip");
                            $zip = new ZipArchive();
                            if (!$zipFile || $zip->open($zipFile, ZipArchive::OVERWRITE) !== true)
                                sendError('', array('msg' => 'No Content', 'code' => 204));

                            while ($install = $installations->fetch_assoc()) {
                                if ($install['photo_before'] != '') {
                                    $base64data = explode(',', substr($install['photo_before'], 5), 2);
                                    $mime = explode('/', explode(';', $base64data[0])[0], 2)[1];
                                    $zip->addFromString('L' . $install['posicion'] . 'A.' . ($mime == 'jpeg' ? 'jpg' : $mime), base64_decode($base64data[1]));
                                }
                                if ($install['photo_after']) {
                                    $base64data = explode(',', substr($install['photo_after'], 5), 2);
                                    $mime = explode('/', explode(';', $base64data[0])[0], 2)[1];
                                    $zip->addFromString('L' . $install['posicion'] . 'LED.' . ($mime == 'jpeg' ? 'jpg' : $mime), base64_decode($base64data[1]));
                                }
                            }

                            $zip->close();

                            if (!db_fn_query("INSERT INTO download_log (ip, id_user, file, tmpfile) VALUES ('" . $_SERVER['REMOTE_ADDR'] . "', " . (isset($data['user']) ? intval($data['user']) : 0) . ", '" . $zipName . "', '" . $zipFile . "')")) {
                                unlink($zipFile);
                                sendError('', array('msg' => 'Gone', 'code' => 410));
                            }
                            echo $zipFile;
                            break;

                        case 'report':
                            $installations = db_fn_query("SELECT * FROM reports WHERE STR_TO_DATE(fecha_instalacion, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
                            if (!$installations)
                                sendError('', array('msg' => 'No Content', 'code' => 204));
                            if ($installations->num_rows == 0)
                                sendError('', array('msg' => 'No Content', 'code' => 204));

                            $fileName = 'Reporte_' . date('Ymd', $data['startDate']) . '-' . date('Ymd', $data['endDate']) . '.csv';
                            $tmpFile = tempnam(sys_get_temp_dir(), "csv");

                            $srcFile = fopen($tmpFile, 'w');
                            $headersAdded = false;
                            while ($installation = $installations->fetch_assoc()) {
                                $coords = json_decode($installation['coordenadas'], true);
                                $commentTemp['comentarios'] = $installation['comentarios'];
                                unset($installation['comentarios']);
                                if ($installation['coordenadas'] != '') {
                                    if ($coords === null) {
                                        $coords = json_decode(preg_replace(array('/(latitude)/', '/(longitude)/'), '"\1"', $installation['coordenadas']), true);
                                        db_fn_query("UPDATE reports SET coordenadas='" . preg_replace(array('/(latitude)/', '/(longitude)/'), '"\1"', $installation['coordenadas']) . "' WHERE posicion='" . $installation['posicion'] . "'");
                                    }
                                    $installation = array_merge($installation, $coords, $commentTemp);
                                } else
                                    $installation = array_merge($installation, array('longitude'=>'', 'latitude'=>''), $commentTemp);
                                unset($installation['coordenadas']);

                                if (!$headersAdded) {
                                    $headersAdded = true;
                                    fputcsv($srcFile, array_keys(array_change_key_case($installation, CASE_UPPER)));
                                }
                                fputcsv($srcFile, $installation);
                            }
                            fclose($srcFile);

                            if (!db_fn_query("INSERT INTO download_log (ip, id_user, file, tmpfile) VALUES ('" . $_SERVER['REMOTE_ADDR'] . "', " . (isset($data['user']) ? intval($data['user']) : 0) . ", '" . $fileName . "', '" . $tmpFile . "')")) {
                                unlink($tmpFile);
                                sendError('', array('msg' => 'Gone', 'code' => 410));
                            }
                            echo $tmpFile;
                            break;

                        default:
                            sendError();
                            break;
                    }
                } else
                    sendError();
            } else
                sendError('La información esta incompleta.');
            break;
        case "PUT":
        case "DELETE":
        default:
            sendError('', array('msg' => 'Method Not Allowed', 'code' => 405));
            break;
    }
} else
    sendError('', array('msg' => 'Unauthorized', 'code' => 401));
?>