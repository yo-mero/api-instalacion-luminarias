<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/siil.inc';

    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET, PUT');
            break;
        case "GET":
            if (isset($_GET['action'])) {
                // Process Action
                switch ($_GET['action']) {
                    case "all":
                        $response = [];
                        // Get Request's row version
                        $requestRowVersion = 0;
                        $latestRowVersion = "";
                        if (isset($_GET['row_version']) && is_numeric($_GET['row_version']))
                            $requestRowVersion = date("Y-m-d H:i:s", $_GET['row_version']);
                        allMoves($response, $requestRowVersion, $latestRowVersion);

                        header('Access-Control-Allow-Origin: *');
                        echo json_encode($response);
                        break;
                    case "range":
                        if (isset($_GET['dateMin'])) {
                            $dateMax = isset($_GET['dateMax']) ? $_GET['dateMax'] : $_GET['dateMin'];
                            rangedateMoves($_GET['dateMin'], $dateMax);
                        }
                        break;
                    case "cuad":
                        cuadrillas();
                        break;
                    default:
                        header("HTTP/1.0 400 Bad Request", true, 400);
                        break;
                }
            }
            break;
        case "PUT":
            header('Access-Control-Allow-Origin: *');
            $data = json_decode(file_get_contents('php://input'), true);
            if (count($data) == $_GET['rows_to_update']) {
                if ($_GET['rows_to_update'] > 0 && !updateMoves($data)) {
                    header("HTTP/1.0 400 Bad Request", true, 400);
                    echo json_encode(array('message' => 'Hubo un error al actualizar las filas.'));
                }
            } else {
                header("HTTP/1.0 400 Bad Request", true, 400);
                echo json_encode(array('message' => 'La longitud de información no coincide con el número de filas a actualizar.'));
            }
            break;
        case "POST":
        case "DELETE":
        default:
            header("HTTP/1.0 405 Method Not Allowed", true, 405);
            die();
            break;
    }

    function allMoves(&$response, $requestRowVersion, &$latestRowVersion)
    {
        // Select all moves with date less or same to current day and created/modified date greater than row_version
        $today = date("Y-m-d");

        if (isset($_GET['local_moves_numrows']) && intval($_GET['local_moves_numrows']) > 0) {
            $total_in_db = db_fn_query("SELECT COUNT(*) AS total FROM mobile_moves WHERE work_date <= '" . $today . "'")->fetch_object()->total;
            if ($total_in_db != intval($_GET['local_moves_numrows']))
                $requestRowVersion = date("Y-m-d H:i:s", 0);
        }
        $db_result = db_fn_query("SELECT * FROM mobile_moves 
                                  WHERE row_version > '" . $requestRowVersion . "' AND work_date <= '" . $today . "'");
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // Look for newest row version
                $latestRowVersion = max($db_row['row_version'], $latestRowVersion);
                // unset row versions and unused id's so they don't appear in response
                //unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }

        // Convert response to json and echo it
        return json_encode($response);
    }

    function rangedateMoves($dateMin, $dateMax)
    {
        if ($dateMin > $dateMax)
            $dateMax = $dateMin;
        $db_result = db_fn_query("SELECT id, name FROM mobile_moves 
                                        WHERE date >= " . $dateMin . " AND date <= " . $dateMax);
        $response = [];

        // Fill response buffer with each database entry
        if ($db_result) {
            while ($db_row = $db_result->fetch_assoc()) {
                // unset row versions and unused id's so they don't appear in response
                unset($db_row['row_version']);

                // Clean corresponding types
                $db_row['id'] = intval($db_row['id']);

                // Push result to response
                array_push($response, $db_row);
            }
        }
        // Convert response to json and echo it
        echo json_encode($response);
    }

    function updateMoves($moves)
    {
        $result = true;
        for ($i = 0; $i < count($moves); $i++) {
            $db_result = db_fn_query("UPDATE mobile_moves SET 
                                      name='" . $moves[$i]['name'] . "', 
                                      id_group=" . intval($moves[$i]['id_group']) . ",  
                                      work_date='" . $moves[$i]['work_date'] . "', 
                                      list_poles='" . $moves[$i]['list_poles'] . "', 
                                      installed_poles='" . $moves[$i]['installed_poles'] . "', 
                                      list_lamps='" . $moves[$i]['list_lamps'] . "', 
                                      installed_lamps='" . $moves[$i]['installed_lamps'] . "',   
                                      row_version='" . date("Y-m-d H:i:s") . "' 
                                      WHERE id=" . $moves[$i]['id']);
            if (!$db_result) {
                $result = false;
            }
        }
        return $result;
    }

    if ($_SESSION['db_oracle'] != false) {
        // Close the Oracle connection
        oci_close($_SESSION['db_oracle']);
    }
?>
