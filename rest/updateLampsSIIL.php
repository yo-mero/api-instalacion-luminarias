<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/siil.inc';

    /**
     * This is the procedure to update the lamps data, using the serial number,
     * in the database of SIIL it saves the position number and
     * coordinates (latitude, longitude) where the lamps were installed
     */
    $dateRowVersionGreaterThan = date('Y-m-d 00:00:00');

    /* Get the list of lamps not installed but loaded or uninstalled in the current day (date at 0 hours server time) */
    $db_lamps_no_installed  = db_fn_query("SELECT serial FROM mobile_lamps WHERE installed=0 AND row_version>='" . $dateRowVersionGreaterThan . "'");
    if ($db_lamps_no_installed) {
        while ($lamp = $db_lamps_no_installed->fetch_assoc()) {
            $oc_result = oracle_query("SELECT id FROM numeros_serie WHERE numero_serie = '" . $lamp['serial'] . "' AND circuito_pos IS NOT NULL");
            if ($oc_result) {
                while ($row = oci_fetch_assoc($oc_result))
                    oracle_query("UPDATE numeros_serie SET circuito_pos = '' , latitud = '' , longitud = ''  WHERE id = " . $row['ID']);
            }
        }
    }
    /* Get the list of groups (almacenes, cuadrillas) */
    $db_groups = db_fn_query("SELECT id FROM mobile_groups ORDER BY id");
    if ($db_groups) {
        // Loop for each group
        while ($group = $db_groups->fetch_object()->id) {
            /* Get the list of installations for the group by current day (date at 0 hours server time) */
            $db_installations = db_fn_query("SELECT id_pole, id_lamp, gps_coords FROM mobile_installation WHERE row_version>='" . $dateRowVersionGreaterThan . "' AND id_move=" . $group . " LIMIT 1");
            if ($db_installations && $db_installations->num_rows > 0) {
                /* If we found installations and a session for procedure wasn't initialize: Start the session */
                if (!isset($_SESSION['session_traspaso']) || $_SESSION['session_traspaso'] <= 0)
                    startSession($group); // Start session for procedure

                // if $_SESSION['session_traspaso'] is not greater than 0 then the session doesn't exists and can't continue
                if ($_SESSION['session_traspaso'] > 0) {
                    $insertQuery = "BEGIN :result := PKG_MEXILED.FNC_AGREGAR_NS( :p_session, :p_numero_serie, :p_posicion, :p_latitud, :p_longitud ); END;"; // Query to cache the lamp data in procedure

                    // Loop for each installation
                    while ($installation = $db_installations->fetch_assoc()) {
                        // Decode the JSON for coordinates
                        $installation['gps_coords'] = str_replace('latitude', '"latitude"', $installation['gps_coords']);
                        $installation['gps_coords'] = str_replace('longitude', '"longitude"', $installation['gps_coords']);
                        $installation['gps_coords'] = json_decode($installation['gps_coords']);

                        /* Set the data in an array of parameters to run the $insertQuery */
                        $posicion = db_fn_query("SELECT name FROM mobile_poles WHERE id=" . $installation['id_pole'])->fetch_object()->name;
                        $variables = array(
                            ':result' => '00',
                            ':p_session' => $_SESSION['session_traspaso'],
                            ':p_numero_serie' => db_fn_query("SELECT serial FROM mobile_lamps WHERE id=" . $installation['id_lamp'])->fetch_object()->serial,
                            ':p_posicion' => $posicion,
                            ':p_latitud' => $installation['gps_coords']->latitude,
                            ':p_longitud' => $installation['gps_coords']->longitude
                        );

                        /* Run the $insertQuery with parameters */
                        oracle_query($insertQuery, $variables);

                        // Catch the result/response and validate
                        if (intval($variables[':result']) !== 1) {
                            switch (intval($variables[':result'])) {
                                case -1:
                                    $message = 'Error de Oracle contactar a soporte';
                                    echo $message . '<br>';
                                    break;
                                case -2:
                                    $message = 'No se encontró la sesión indicada';
                                    echo $message . '<br>';
                                    break;
                                case -3:
                                    $message = 'No se encontró el número de serie indicado';
                                    echo $message . '<br>';
                                    break;
                                case -4:
                                    $message = 'Debe de especificar la posición';
                                    echo $message . '<br>';
                                    break;
                                case -5:
                                    $message = 'Debe de especificar latitud del Número de Serie';
                                    echo $message . '<br>';
                                    break;
                                case -6:
                                    $message = 'Debe de especificar longitud del Número de Serie';
                                    echo $message . '<br>';
                                    break;
                                case -7:
                                    $message = 'El almacén origen del NS es diferente al de los otros de la lista';
                                    echo $message . '<br>';
                                    break;
                                default:
                                    $message = 'Error desconocido al agregar un NS';
                                    echo $message . ' ' . $variables[':p_numero_serie'] . '<br>';
                                    break;
                            }
                            saveLog('Insert', $message, $variables);
                        }
                    }

                    /* Once the loop for installations ended and the lamps data are cached, End the session to save the data */
                    $endQuery = "BEGIN :result := PKG_MEXILED.FNC_TERMINAR_TRASPADO( :p_session ); END;"; // End session query
                    /* Array of parameters to run the $endQuery */
                    $variables = array(
                        ':result' => '00',
                        ':p_session' => $_SESSION['session_traspaso']
                    );
                    /* Run $endQuery with parameters */
                    oracle_query($endQuery, $variables);
                    // Catch the result/response and validate
                    if (intval($variables[':result']) !== 1) {
                        switch (intval($variables[':result'])) {
                            case -1:
                                $message = 'Error de Oracle contactar a soporte';
                                echo $message;
                                unset($_SESSION['session_traspaso']);
                                break;
                            case -2:
                                $message = 'No se encontró la sesión indicada';
                                echo $message;
                                unset($_SESSION['session_traspaso']);
                                break;
                            case -3:
                                $message = 'La sesión no tiene números de serie asignados para traspaso';
                                echo $message;
                                break;
                            case -4:
                                $message = 'Hay más de un almacén origen. Todos los números de series debe de pertenecer al mismo almacén';
                                echo $message;
                                break;
                            case -5:
                                $message = 'Ocurrió un error al realizar el traspaso de un número de serie';
                                echo $message;
                                break;
                            case -6:
                                $message = 'Ocurrió un error al crear el encabezado del traspaso';
                                echo $message;
                                break;
                            default:
                                $message = 'Error desconocido al finalizar traspaso';
                                echo $message;
                                break;
                        }
                        saveLog('End', $message, $variables);
                    } else {
                        $message = 'La sesión de registro para cuadrilla ' . $group . ' finalizo, ' . $db_installations->num_rows . ' lamparas actualizadas';
                        echo $message;
                        unset($_SESSION['session_traspaso']);
                        saveLog('OK', $message, $variables);
                    }
                }
            }
        }
    }

    /**
     * Function to start the session of the procedure and saving data into SIIL DB
     * @param $almacen <int> group/almacen/cuadrilla ID, must be an integer this is validated
     */
    function startSession($almacen)
    {
        if (is_numeric($almacen) && ($almacen = intval($almacen)) > 0) {
            $startQuery = "BEGIN :result := PKG_MEXILED.FNC_INICIAR_TRASPASO( :p_almacen ); END;"; // Start session query
            /* Array of parameters to run the $startQuery */
            $variables = array(
                ':result' => '00000000000000000000',
                ':p_almacen' => $almacen
            );
            /* Run $startQuery with parameters */
            oracle_query($startQuery, $variables);
            // Catch the session and save in a $_SESSION variable to keep it alive
            $_SESSION['session_traspaso'] = intval($variables[':result']);
            // Validate the result/session, is is not valid send a error message and save the log
            if ($_SESSION['session_traspaso'] < 0) {
                switch ($_SESSION['session_traspaso']) {
                    case -1:
                        $message = 'Error de Oracle al iniciar sesión  de traspaso, contactar a soporte';
                        echo $message . '<br>';
                        saveLog('Start', $message, $variables);
                        break;
                    case -2:
                        $message = 'La clave del almacén no es válida, no se puede iniciar sesión de traspaso';
                        echo $message . '<br>';
                        break;
                    default:
                        $message = 'Hubo un error al iniciar sesión de traspaso';
                        echo $message . '<br>';
                        break;
                }
                saveLog('Start', $message, $variables);
            }
        }
    }

    /**
     * Function to save log in database when an error exists
     * @param $operation <string>
     * @param $message <string>
     * @param $parameters <array>
     */
    function saveLog($operation, $message, $parameters) {
        db_fn_query("INSERT INTO siil_save_procedure_log (operation, message, parameters) VALUES ('" . $operation . "', '" . $message . "', '" . json_encode($parameters) . "')");
    }
?>