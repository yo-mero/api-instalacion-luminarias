<?php
/**
 * Created by IntelliJ IDEA.
 * User: gcuellar
 * Date: 10/10/17
 * Time: 11:32 AM
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/main.inc';
require_once $_SERVER['DOCUMENT_ROOT'] . '/mobile/api/includes/db_functions.inc';

$results = db_fn_query("SELECT * FROM reports");
$file = '';
if ($results) {
    $file .= '<table>
            <thead>
                <tr>
                    <th>ALMACEN</th>
                    <th>ARTICULO</th>
                    <th>SERIE</th>
                    <th>POSICION</th>
                    <th>CIRCUITO</th>
                    <th>FECHA INSTALACION</th>
                    <th>DIRECCION</th>
                    <th>FRENTE NO</th>
                    <th>CALLE 1</th>
                    <th>CALLE 2</th>
                    <th>TIPO</th>
                    <th>POTW</th>
                    <th>BAL</th>
                    <th>LONGITUD</th>
                    <th>LATITUD</th>
                    <th>COMENTARIOS / OBSERVACIONES</th>
                </tr>
            </thead>
            <tbody>';

    $results = $results->fetch_all(MYSQLI_ASSOC);
    foreach ($results as $key => $db_row) {
        $coords = json_decode($db_row['coordenadas'], true);
        $commentTemp['comentarios'] = $db_row['comentarios'];
        unset($db_row['comentarios']);
        if ($db_row['coordenadas'] != '') {
            if ($coords === null) {
                $coords = json_decode(preg_replace(array('/(latitude)/', '/(longitude)/'), '"\1"', $db_row['coordenadas']), true);
                db_fn_query("UPDATE reports SET coordenadas='" . preg_replace(array('/(latitude)/', '/(longitude)/'), '"\1"', $db_row['coordenadas']) . "' WHERE posicion='" . $db_row['posicion'] . "'");
            }
            $db_row = array_merge($db_row, $coords, $commentTemp);
        } else
            $db_row = array_merge($db_row, array('longitude'=>'', 'latitude'=>''), $commentTemp);
        unset($db_row['coordenadas']);
        $results[$key] = $db_row;

        $file .= '<tr>
                <td>' . $db_row['almacen'] . '</td>
                <td>' . $db_row['articulo'] . '</td>
                <td>' . $db_row['serie'] . '</td>
                <td>' . $db_row['posicion'] . '</td>
                <td>' . $db_row['circuito'] . '</td>
                <td>' . $db_row['fecha_instalacion'] . '</td>
                <td>' . $db_row['ubicacion'] . '</td>
                <td>' . $db_row['frente_no'] . '</td>
                <td>' . $db_row['calle_1'] . '</td>
                <td>' . $db_row['calle_2'] . '</td>
                <td>' . $db_row['tipo'] . '</td>
                <td>' . $db_row['potw'] . '</td>
                <td>' . $db_row['bal'] . '</td>
                <td>' . $db_row['longitude'] . '</td>
                <td>' . $db_row['latitude'] . '</td>
                <td>' . $db_row['comentarios'] . '</td>
            </tr>';
    }

    $file .= '</tbody>
        </table>';

// disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

// force download
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

// disposition / encoding on response body
    $filename = "Reporte_posiciones_" . date("Y-m-d") . ".csv";
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");

    echo array2csv($results);
    die();
}

function array2csv(array &$array)
{
    if (count($array) == 0)
        return null;
    ob_start();
    $df = fopen("php://output", 'w');
    fputcsv($df, array_keys(reset($array)));
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}
?>