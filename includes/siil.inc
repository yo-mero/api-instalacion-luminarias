<?php
    require_once $_SERVER['DOCUMENT_ROOT'].'/mobile/api/includes/main.inc';

    define('SIIL_HOST', '34.204.16.221');
    define('SIIL_SERV', 'dbsiil');
    if (MAIN_AMBIENT_TYPE !== "") {
        define('SIIL_USER', 'siil_demo2');
        define('SIIL_PASS', 'demo#mexiled');
    } else {
        define('SIIL_USER', 'siil_owner2');
        define('SIIL_PASS', 'luthe#mexiled');
    }

    $_SESSION['db_oracle'] = oracle_connect();

    // Create connection to Oracle
    function oracle_connect()
    {
        $db_connection = oci_connect(SIIL_USER, SIIL_PASS, '//' . SIIL_HOST . '/' . SIIL_SERV);
        if (!$db_connection) {
            $e = oci_error();
            die(htmlentities('Error de Conexión Oracle ' . $e['message'], ENT_QUOTES));
        }

        return $db_connection;
    }

    function oracle_query($query, &$bind_variables = [])
    {
        if ($query != "") {
            $db_conn = $_SESSION['db_oracle'];

            $stid = oci_parse($db_conn, $query);
            if (count($bind_variables) > 0) {
                foreach ($bind_variables as $key => $value) {
                    oci_bind_by_name($stid, $key, $bind_variables[$key]);
                }
            }

            if (!oci_execute($stid)) {
                return false;
            }

            return $stid;
        } else
            return false;
    }
?>