<?php
    require_once $_SERVER['DOCUMENT_ROOT'].'/mobile/api/includes/main.inc';
    require_once $_SERVER['DOCUMENT_ROOT'].'/mobile/api/includes/db_types.inc';
    require_once $_SERVER['DOCUMENT_ROOT'].'/mobile/api/includes/generic_functions.inc';
    
    define("DB_USE_BACKUP", false);
    define("DB_SERVER_MAIN", $_SERVER['SERVER_ADDR']);
    define("DB_SERVER_BACKUP", ($_SERVER['SERVER_ADDR'] == MAIN_SERVER ? BACKUP_SERVER : MAIN_SERVER) );

    $DB_USER = "mexiled_instal";
    $DB_PASS = "M3x1L3d#";
    $DB_NAME = "mexiled_installation".MAIN_AMBIENT_TYPE;
    
    if (isset($_SESSION['db_conn_main'])) { // 
        $db_connection = fixObject($_SESSION['db_conn_main']);
        if (is_resource($db_connection))
            $db_connection->close();
    }
    $_SESSION['db_conn_main'] = db_fn_connect(DB_SERVER_MAIN);
    
    
    if (DB_USE_BACKUP == true){
        try {
            if (isset($_SESSION['db_conn_backup'])) { // 
                $db_connection = fixObject($_SESSION['db_conn_backup']);
                if (is_resource($db_connection))
                    $db_connection->close();
            }
            $_SESSION['db_conn_backup'] = db_fn_connect(DB_SERVER_BACKUP);
        } catch (Exception $e) {
            // Enviar correo ¿?
        }
    }
    
    function db_fn_connect($address)
    {
        $db_connection = new mysqli("localhost", $GLOBALS['DB_USER'], $GLOBALS['DB_PASS']);
        
        if ($db_connection->connect_error)
        {
            die('Error de Conexión (' . $db_connection->connect_errno . ') '. $db_connection->connect_error);
        }

        if ($db_connection->select_db($GLOBALS['DB_NAME']) == false)
        {
            //Database doesn't exist, create database
            $sql="CREATE DATABASE ".$GLOBALS['DB_NAME'];
            if ($db_connection->query($sql) == false)
            {
                die('Error (' . $db_connection->errno . ') '. $db_connection->error);
            }

            if ($db_connection->select_db($GLOBALS['DB_NAME']) == false)
            {
                die('Error (' . $db_connection->errno . ') '. $db_connection->error);
            }
        }

        return $db_connection;
    }
    
    
    
    function db_fn_query($query)
    {
        $db_conn = $_SESSION['db_conn_main'];
        $db_conn->query('SET CHARACTER SET utf8');
        $db_result = $db_conn->query($query);

        if (!$db_result) {
            printf("Error: %s\nQuery: %s\n", $db_conn->error, $query);
            return false;
        }

        if ( (DB_USE_BACKUP == true) && (isset($_SESSION['db_conn_backup'])) ){
            if (strtoupper(substr($query, 0, 6)) != "SELECT"){
                if (is_resource($_SESSION['db_conn_backup'])){
                    $db_conn = $_SESSION['db_conn_backup'];
                    $db_conn->query($query);
                }
            }
        }
    
        return $db_result;
    }
    
    function db_fn_affected_rows()
    {
        $db_conn = $_SESSION['db_conn_main'];
        return $db_conn->affected_rows;
    }
    
    function db_fn_errno()
    {
        $db_conn = $_SESSION['db_conn_main'];
        return $db_conn->errno;
    }
    
    function db_fn_error()
    {
        $db_conn = $_SESSION['db_conn_main'];
        return $db_conn->error;
    }
    
    function db_fn_insert_id()
    {
        $db_conn = $_SESSION['db_conn_main'];
        return $db_conn->insert_id;
    }
?>
