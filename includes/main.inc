<?php
/***********************************************
* This file contains all the include functions *
* which shall always be included in any file   *
***********************************************/

    define("MAIN_SERVER", "209.126.99.20");
    define("BACKUP_SERVER", "");

    define('MAIN_AMBIENT_TYPE', (strpos(fn_getCurPageURL(), "demo") != false ? "_demo" : (strpos(fn_getCurPageURL(), "develop") != false ? "_dev" : "")) );  // Use "_dev" for development, "_demo" for demo or "" for production
    define('CURR_DOMAIN', ""); //For use in case of subdomains
    define('COOKIE_NAME', 'MXLED'.MAIN_AMBIENT_TYPE);

    define('COOKIE_TIMEOUT', 600);            // Cookie timeout in minutes
    
    define('API_TOKEN', 'ABCD1234');
    
    date_default_timezone_set('America/Mexico_City');
    setlocale(LC_ALL, 'es_MX');
    setlocale(LC_MONETARY, 'es_MX');
    setlocale(LC_NUMERIC, 'es_MX');

    session_start();

    function fn_session_start()
    {
        // Set expire time
        //setcookie(COOKIE_NAME, "TRUE", time() + (COOKIE_TIMEOUT * 60), "/");
    }

    function fn_session_clear()
    {
        // Set expire time
        if(!isset($_SESSION))
        {
            session_start();
        }
        session_unset();
        session_destroy();
        session_write_close();
        setcookie(COOKIE_NAME, "", time() - 1000, "/");
        unset($_COOKIE[COOKIE_NAME]);
    }

    function fn_session_update()
    {
        fn_session_start();
    }

    function fn_isSecurePage()
    {
        $curr_page = fn_removeDomain(fn_getCurPageURL());
        $curr_page = fn_removeURLInfo($_SERVER['PHP_SELF']);

        if (   $curr_page != CURR_DOMAIN."index.php?logout"
            && $curr_page != CURR_DOMAIN."index.php"
            && $curr_page != CURR_DOMAIN
            && $curr_page != CURR_DOMAIN."cronJobs/HdCDuplicadas.php"
            && $curr_page != CURR_DOMAIN."initial_setup.php"
            && $curr_page != CURR_DOMAIN."rest/dbConnection.php")
        {
            // This web page requires user authentication (user and password)
            return true;
        }
        else
        {
            // This web page does not require user tu be authenticated
            return false;
        }
    }

    function fn_removeDomain($curr_page)
    {
        $curr_page_wo_domain = "";
        
        for ($i = 0; $i < strlen($curr_page) - strlen(CURR_DOMAIN); $i++)
        {
            if(substr($curr_page, $i, strlen(CURR_DOMAIN)) == CURR_DOMAIN)
            {
                $scrap_len = $i + strlen(CURR_DOMAIN) +1;
                $curr_page_wo_domain = substr($curr_page, $scrap_len, strlen($curr_page) - $scrap_len);
            }
        }
        
        return $curr_page_wo_domain;
    }

    function fn_removeURLInfo($curr_page)
    {
        $curr_page_wo_info = "";
        $ret_val = "";
        
        for ($i = 0; $i < strlen($curr_page); $i++)
        {
            if(substr($curr_page, $i, 3) == "php")
            {
                $ret_val = substr($curr_page,0,$i + 3);
            }
        }
        
        if ($ret_val == "")
        {
            $ret_val = $curr_page;
        }
        
        if (substr($ret_val,0,1) == "/")
        {
            $ret_val = substr($ret_val, 1, strlen($ret_val) - 1);
        }
        
        return $ret_val;
    }
    
    function fn_isDemoUser()
    {
        if (($_SESSION['account_type'] == "DEMO_ONLY") || (substr($_SESSION['user'],0,5) == "demo_"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function fn_getCurPageURL()
    {
        $pageURL = 'http';
        
        if (isset( $_SERVER["HTTPS"] ))
        {
            if ($_SERVER["HTTPS"] == "on")
            {
                $pageURL .= "s";
            }
            }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80")
        {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        }
        else
        {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        
        return $pageURL;
    }

    function validateRequest(&$get, &$post){
        if (isset($get['apiToken'])){
            if ($get['apiToken'] == API_TOKEN){
                return true;
            }
        } 
        header('HTTP/1.1 401 Unauthorized', true, 401);
        die();
    }

    if ($_SERVER['REMOTE_ADDR'] != MAIN_SERVER)
    {
        validateRequest($_GET, $_POST);
    }

?>
